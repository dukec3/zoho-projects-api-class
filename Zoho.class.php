<?php

/**
 * @filename: Zoho.class.php
 * @description: This file implements Zoho Project API's to interact with the zoho portal
 * @author: Christopher Duke <christopher.duke@unlv.edu>
 * @author David Riccardi <driccardi@gmail.com>
 * @license: http://www.apache.org/licenses/LICENSE-2.0 Apache 2.0 License
 * 		 Copyright 2012 David Riccardi
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * @update: 2013-01-10
 * @requirements: PHP server must have cURL installed < http://curl.haxx.se/ >
 *
 *
 * */

/**
 * class Zoho
 * This class handles all of the methods related to communication with the Zoho Projects API.
 * @package Zoho
 */
class Zoho {

//********************* public parameters ******* 
    /**
     * Stores any errors generated in class usage. Always check for count > 0 before returing a result to the client.
     * @var array
     * */
    public $errors = array();

    /**
     * The current API Ticket
     * @var string
     * */
    public $authtoken;
    public $portal;
    public $user;
    public $pass;

    /**
     * The Login Id string of the credentialed Zoho User (the user called to instantiate the class).
     * This login id is used as a default in all methods that require one unless specified otherwise.
     * @var string
     * */
    public $loginId;

    /**
     * The base path for the zoho api - see constructor for creation
     * @var string
     * */
    private $base_url;

//*** private parameters *****
//** **************** constructor *********************

    /**
     * 	Constructor instantiates the class, sets defaults, get or creates a new ticket.
     * @param string $user Same requirements as the default user (must be a Zoho user in this portal)
     * @param string $pass Cleartext password for the user
     * @param string $portal portal to default to
     * @return void
     * */
    function __construct($user, $pass, $portal, $authtoken = NULL) {

        $this->user = $user;
        $this->pass = $pass;

        $this->portal = $portal;

        $this->authtoken = (is_null($authtoken)) ? $this->getAuthtoken() : $authtoken;

        //while this does setup a based url some methods still have a directed path - getPortals and getUser
        $this->base_url = "https://projectsapi.zoho.com/restapi";
    }

// *****************   public methods ******************************
    /**
     * get some details about the user connected to this class instance
     * @return object User object from Zoho API (of current credentialed user)
     * */
    public function getUser() {
        $url = "https://projectsapi.zoho.com/api/private/json/dashbs/getlogin?authtoken=" . $this->authtoken;
        $post = array();
        $res = $this->post_data($url, $post);
        return json_decode($res);
    }

    /**
     * get the login ID of the current user
     * @return string
     * */
    public function getLoginId() {
        $r = $this->getUser();
        return $r->response->result->login_id;
    }

    /**
     * Get some details about the portal(s) that the user is credentialed for
     * @return object || string  If $nameOnly = TRUE, return string, else returns the Portal Object
     * */
    public function getPortals() {
        $url = "/portals/";
        $res = json_decode($this->request_data($url, 'GET', array()));
        return $res;
    }

############################## USER METHODS #############################################
    /**
     * get all of the users in a project. 
     * @param string $projid - The project ID 
     * @return object returns the Zoho API object containing the project User Objects
     * */

    public function getAllUsers($projId) {
        $url = "/portal/" . $this->portal . "/projects/" . $projId . "/users/";
        return json_decode($this->request_data($url, 'GET', array()));
    }

########################## PROJECT METHODS ##############################################
    /**
     * Get all of the projects assigned to the class instance user
     * @param integer $auditIndex - Used for paging results, default to 1
     * @param integer $range - Number of results to return.
     * @return object returns the Zoho API object containing an array of the project objects.
     * */

    public function getProjects($auditIndex = 1, $range = 50, $status = 'active') {
        $data = array("index" => $auditIndex, "range" => $range, "status" => $status);
        $url = "/portal/" . $this->portal . "/projects/";
        return json_decode($this->request_data($url, 'GET', $data));
    }

    /**
     * Get some content details about the project like number of users, time started, description, etc. 
     * @param string $projid - The project ID 
     * @return object returns the Zoho API object containing the project Details
     * */
    public function getProjectContent($projId) {
        $url = $this->base_url . "project/content?authtoken=" . $this->authtoken;

        $post = array("projId" => $projId, "submit" => "Get Project Content");
        return json_decode($this->post_data($url, $post));
    }

    /**
     * Deletes the Project
     * @param string $projid - The project ID 
     * @return boolean returns true on success, false on error (and adds an error to the class error parameter)
     * */
    public function deleteProject($projId) {
        $url = $this->base_url . "project/delete?authtoken=" . $this->authtoken;
        $post = array("projId" => $projId, "submit" => "Delete Projec");
        $result = json_decode($this->post_data($url, $post));
        if ($result->result == 'Project deleted Successfully') {
            return true;
        } else {
            $this->errors[] = $this->user . " tried to delete project ID: " . $projId . ". This action was unsuccessful.";
            return false;
        }
    }

    /**
     * Adds a new project to the portal
     * @param string $name The name (title) of the new project.
     * @param string $desc string a rich text description of the new project. 
     * @return object The resulting project object
     * */
    public function addProject($name, $desc = '') {
        $url = $this->base_url . "project/add?authtoken=" . $this->authtoken;
        $post = array("projTitle" => $name, "projDesc" => $desc);
        return json_decode($this->post_data($url, $post));
    }

################################# MILESTONE METHODS ######################################
    /**
     * Adds a milestone to the project.
     * @param string $projid - The project ID
     * @param string $mTitle - The name of the milestone
     * @param string $loginId - The loginId of the owner of this milestone.
     * @param datetime $startDate - the date the milestone will start (defaults to today)
     * @param datetime $endDate - the date the milestone will end (defaults to tomorrow)
     * @param string $flag = 'internal' or 'external' coorisponding to the milestone type
     * @return object returns the project details for the milestone that was just created
     * */

    public function addMilestone($projId, $mTitle, $loginId = null, $startDate = null, $endDate = null, $flag = "internal") {
        $url = "/portal/" . $this->portal . "/projects/" . $projId . "/milestones/";

        if ($loginId == null) {
            $loginId = $this->loginId;
        } // set default owner to credentialed user
        if ($startDate == null) {
            $startDate = time();
            $startDate = date('m-d-Y', $startDate);
        }
        if ($endDate == null) {
            $endDate = strtotime('+1 day', $startDate);
            $endDate = date('m-d-Y', $endDate);
        }

        if (($flag == 'internal') || ($flag == 'external')) {
            //continue;
        } else {
            $this->errors[] = "Invalid Flag parameter ($flag) in addMilestone method. Must be internal or external.";
            return false;
        }

        $post = array(
            "name" => $mTitle,
            "start_date" => $startDate,
            "end_date" => $endDate,
            "flag" => $flag,
            "owner" => $loginId
        );
        dpm($post);
        
        return json_decode($this->request_data($url, 'POST', $post));
    }

    /**
     * Updates the details about a milestone
     * @param string $projid - The project ID 
     * @param string $mid - The milestone ID 
     * @param string $mTitle - The name of the milestone
     * @param string $loginId - The loginId of the owner of this milestone.
     * @param datetime $startDate - the date the milestone will start (defaults to today)
     * @param datetime $endDate - the date the milestone will end (defaults to tomorrow)
     * @param string $flag = 'internal' or 'external' coorisponding to the milestone type
     * @param string $status = 'completed' or 'notcompleted' corrisponding to the milestone status
     * @return object returns the project details for the milestone that was just created
     * */
    public function updateMilestone($projId, $mid, $mTitle, $loginId = null, $startDate = null, $endDate = null, $flag = "internal", $status = 'notcompleted') {

        $url = $this->base_url . "ms/update?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        if ($loginId == null) {
            $loginId = $this->loginId;
        } // set default owner to credentialed user
        if ($startDate == null) {
            $startDate = time();
        }
        if ($endDate == null) {
            $endDate = strtotime('+1 day', $startDate);
        }
        $startDate = date('m-d-Y', $startDate);
        $endDate = date('m-d-Y', $endDate);
        if (($flag == 'internal') || ($flag == 'external')) {
            //continue;
        } else {
            $this->errors[] = "Invalid Flag parameter ($flag) in updateMilestone method. Must be internal or external.";
            return false;
        }
        if (($status == 'notcompleted') || ($status == 'completed')) {
            
        } else {
            $this->errors[] = 'Invalid status parameter in updateMilestone method. Must be "completed" or "notcompleted".';
            return false;
        }


        $post = array("projId" => $projId,
            "mid" => $mid,
            "mtitle" => $mTitle,
            "mstartdate" => $startDate,
            "msdate" => $endDate,
            "flag" => $flag,
            "status" => $status,
            "owner" => $loginId
        );
        return json_decode($this->post_data($url, $post));
    }

    /**
     * Set the Milestone to Complete
     * @param string $projid - The project ID 
     * @param string $mid - The milestone ID 
     * @param string $status = 'completed' or 'notcompleted' corrisponding to the milestone status (default is completed)
     * @return object returns the milestone details for the action
     * */
    public function setMilestoneComplete($projId, $mid, $status = 'completed') {

        $url = $this->base_url . "ms/setstatus?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        if ($status == 'completed') {
            $status = 2;
        } else {
            $status = 1;
        }

        $post = array("projId" => $projId,
            "mid" => $mid,
            "status" => $status
        );
        return json_decode($this->post_data($url, $post));
    }

    /**
     * Delete a milestone
     * @param string $projid - The project ID 
     * @param string $mid - The milestone ID 
     * @return boolean returns the true on success
     * */
    public function deleteMilestone($projId, $mid) {
        $url = $this->base_url . "ms/delete?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        $post = array("projId" => $projId,
            "mid" => $mid
        );
        $result = json_decode($this->post_data($url, $post));
        if ($result->result == 'Deleted Successfully') {
            return true;
        } else {
            $this->errors[] = $this->user . " tried to delete Milestone ID: " . $mid . ". This action was unsuccessful.";
            return false;
        }
    }

    /**
     * gets all of the milestones for a project using parameters
     * @param string $projid - The project ID 
     * @param int $index - Starting index
     * @param int $range - Number to get no more than 100
     * @param string $flag = 'allflag', 'internal' or 'external' coorisponding to the milestone type
     * @param string $status = 'all', 'completed' or 'notcompleted' corrisponding to the milestone status
     * @param string $display_type = 'all','upcoming' or 'delayed'
     * @return object returns the project details for all milestones matching the criteria
     * */
    public function getMilestones($projId, $index = 0, $range = 100, $flag = 'allflag', $status = 'all', $display_type = 'all') {
        $url = "/portal/" . $this->portal . "/projects/" . $projId . "/milestones/";

        if (($flag == 'internal') || ($flag == 'external') || ($flag == 'allflag')) {
            //continue;
        } else {
            $this->errors[] = "Invalid Flag parameter ($flag) in addMilestone method. Must be internal ,external, or allflag.";
            return false;
        }
        if (($status == 'notcompleted') || ($status == 'completed') || ($status == 'all')) {
            
        } else {
            $this->errors[] = 'Invalid status parameter in addMilestone method. Must be "completed" or "notcompleted".';
            return false;
        }
        if (($display_type == 'upcoming') || ($display_type == 'delayed') || ($display_type == 'all')) {
            
        } else {
            $this->errors[] = 'Invalid matchcrit parameter in addMilestone method. Must be "upcoming" or "delayed".';
            return false;
        }

        $post = array(
            "index" => $index,
            "range" => $range,
            "flag" => $flag,
            "status" => $status,
            "display_type" => $display_type
        );
        $results = json_decode($this->request_data($url, 'GET', $post));
        $milestones = $results->milestones;
        if (count($milestones) == 100 && $range == 100) {
            $milestones = array_merge($milestones, $this->getMilestones($projId, count($milestones) + 1));
        }
        return $milestones;
    }

############################### TASKLIST METHODS #########################################

    /**
     * Add a tasklist to a project/milestone
     * @param string $projid - The project ID 
     * @param string $mid - The milestone ID
     * @param string $title - the name of the tasklist
     * @param string $flag = 'internal' or 'external' coorisponding to the tasklist type
     * @return object the tasklist object that was just created.
     * */
    public function addTasklist($projId, $title, $mid, $flag = 'internal') {
        $url = "/portal/" . $this->portal . "/projects/" . $projId . "/tasklists/";
        if (($flag == 'internal') || ($flag == 'external')) {
            //continue;
        } else {
            $this->errors[] = "Invalid Flag parameter ($flag) in addTasklist method. Must be internal or external.";
            return false;
        }

        $post = array(
            "milestone_id" => $mid,
            "flag" => $flag,
            "name" => $title,
        );

        return json_decode($this->request_data($url, 'POST', $post));
    }

    /**
     * Add a tasklist to a project/milestone
     * @param string $projid - The project ID 
     * @param string $tasklistId - The tasklist to be modified.
     * @param string $mid - The milestone ID
     * @param string $title - the name of the tasklist
     * @param string $flag = 'internal' or 'external' coorisponding to the tasklist type
     * @return object the tasklist object that was just created.
     * */
    public function updateTasklist($projId, $tasklistId, $title, $mid, $flag = 'internal', $status = 'active') {
        $url = $this->base_url . "tasklist/update?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        if (($flag == 'internal') || ($flag == 'external')) {
            //continue;
        } else {
            $this->errors[] = "Invalid Flag parameter ($flag) in addTasklist method. Must be internal or external.";
            return false;
        }
        if (($status == 'active') || ($status == 'completed')) {
            
        } else {
            $this->errors[] = 'Invalid status parameter in addTasklist method. Must be "active" or "completed".';
            return false;
        }

        $post = array("projId" => $projId,
            "taskid" => $tasklistId,
            "mStone" => $mid,
            "flag" => $flag,
            "tasklist" => $title,
            "status" => $status
        );
        Return json_decode($this->post_data($url, $post));
    }

    /**
     * Remove a tasklist (and all associated tasks)
     * @param string $projid - The project ID 
     * @param string $tasklistId - The tasklist to be deleted
     * @return boolean return true on success.
     * */
    public function deleteTasklist($projId, $tasklistId) {
        $url = $this->base_url . "tasklist/delete?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        $post = array("projId" => $projId,
            "taskid" => $tasklistId
        );
        $result = json_decode($this->post_data($url, $post));
        if ($result->result == 'Deleted Successfully') {
            return true;
        } else {
            $this->errors[] = $this->user . " tried to delete Tasklist ID: " . $tasklistId . ". This action was unsuccessful.";
            return false;
        }
    }

    /**
     * Get all taskslists associated with this project
     * @param string $projid - The project ID 
     * @param string $flag = 'allflag', 'internal' or 'external' coorisponding to the tasklist type
     * @param integer $auditIndex - Starting record for returning tasklists (to be used in paging results)
     * @param integer $range - Number of results to return (to be used in paging results)
     * @return object the tasklist objects
     * */
    public function getTasklists($projId, $flag = 'internal', $auditIndex = 0, $range = 100) {
        $url = "/portal/" . $this->portal . "/projects/" . $projId . "/tasklists/";
        if (($flag == 'internal') || ($flag == 'external')) {
            //continue;
        } else {
            $this->errors[] = "Invalid Flag parameter ($flag) in addTasklist method. Must be internal or external.";
            return false;
        }
        $post = array(
            "flag" => $flag,
            "index" => $auditIndex,
            "range" => $range
        );
        $results = json_decode($this->request_data($url, 'GET', $post));
        $tasklists = $results->tasklists;
        if (count($tasklists) == 100 && $range == 100) {
            $tasklists = array_merge($tasklists, $this->getTasklists($projId, $flag, count($tasklists) + 1));
        }
        return $tasklists;
    }

############################## TASK METHODS ##############################################

    /**
     * adds a task to a tasklist within a project
     * @param string $projid - The project ID
     * @param string $tasklistID - The ID for the tasklist
     * @param string $name - the name of the task to be added
     * @param string $loginId - Owner ID of the task. User ID for multiple owners must be separated by commas.
     * @param datetime $startDate - the date the task will start (defaults to today)
     * @param datetime $endDate - the date the task will end (defaults to today)
     * @param integer $duration - the number of days the task will take
     * @param string $priority - how important is this task (None | Low | High | Medium) 
     * @return object returns the project details for the task that was just created
     * */
    public function addTask($projId, $tasklistId, $name, $loginId = null, $startDate = null, $endDate = null, $duration = '1', $priority = 'None') {
        $url = "/portal/" . $this->portal . "/projects/" . $projId . "/tasks/";
        if ($loginId == null) {
            $loginId = $this->loginId;
        } // set default owner to credentialed user
        if ($startDate == null) {
            $startDate = time();
        }
        $startDate = date('m-d-Y', $startDate);
        if ($endDate == null) {
            $endDate = time();
        }
        $endDate = date('m-d-Y', $endDate);

        if (($priority == 'None') || ($priority == 'Low') || ($priority == 'Medium') || ($priority == 'High')) {
            //continue;
        } else {
            $this->errors[] = "Invalid Priority parameter ($priority) in addTask method. Must be (None | Low | High | Medium).";
            return false;
        }

        $post = array(
            "name" => $name,
            "person_responsible" => $loginId,
            "tasklist_id" => $tasklistId,
            "start_date" => $startDate,
            "end_date" => $endDate,
            "duration" => $duration, // duration is in days
            "priority" => $priority
        );
        return json_decode($this->request_data($url, 'POST', $post));
    }

    /**
     * adds a task to a tasklist within a project
     * @param string $projid - The project ID
     * @param string $taskID - The ID for the task
     * @param string $taskDesc - the description of the task to be added
     * @param string $loginId - The loginId of the owner of this task.
     * @param [datetime] $startDate - the date the task will start (defaults to today)
     * @param integer $duration - the number of days the task will take
     * @param string $flag = 'internal' or 'external' coorisponding to the milestone type
     * @param string $pComplete - How complete is this task already
     * @param string $priority - how important is this task (None | Low | High | Medium) 
     * @return object returns the project details for the task that was just Updated
     * */
    public function updateTask($projId, $taskId, $taskDesc, $loginId = null, $startDate = null, $duration = '1', $pComplete = '0', $priority = 'None') {
        $url = $this->base_url . "task/update?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        if ($loginId == null) {
            $loginId = $this->loginId;
        } // set default owner to credentialed user
        if ($startDate == null) {
            $startDate = time();
        }
        $startDate = date('m-d-Y', $startDate);
        if (($priority == 'None') || ($priority == 'Low') || ($priority == 'Medium') || ($priority == 'High')) {
            //continue;
        } else {
            $this->errors[] = "Invalid Priority parameter ($priority) in updateTask method. Must be (None | Low | High | Medium).";
            return false;
        }
        $pComplete = $this->cast_pcomplete($pComplete);
        $post = array("projId" => $projId,
            "personresponsible" => $loginId,
            "taskid" => $taskId,
            "todotask" => $taskDesc,
            "taskdate" => $startDate,
            "pcomplete" => $pComplete,
            "duration" => $duration, // duration is in days
            "priority" => $priority
        );
        return json_decode($this->post_data($url, $post));
    }

    /**
     * Remove a task
     * @param string $projid - The project ID 
     * @param string $taskId - The task to be deleted
     * @return boolean return true on success
     * */
    public function deleteTask($projId, $taskId) {
        $url = $this->base_url . "task/delete?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        $post = array("projId" => $projId,
            "taskid" => $taskId
        );
        $result = json_decode($this->post_data($url, $post));
        //return json_decode($this->post_data($url,$post));
        if ($result->response->result == 'Deleted Successfully') {
            return true;
        } else {
            $this->errors[] = $this->user . " tried to delete Task ID: " . $taskId . ". This action was unsuccessful.";
            return false;
        }
    }

    /**
     * Get all tasks associated with this project
     * @param string $projid - The project ID 
     * @param string $flag = 'allflag', 'internal' or 'external' coorisponding to the task type
     * @param string $uname - 'all' or the loginId of the individual user.
     * @param string $mstatus - milestone status that the tasks are related to (completed|notcompleted)
     * @param string $dispType - which tasks to get (upcoming|delayed|misc)
     * @return object the tasklist objects 
     * */
    public function getTasks($projId, $flag = 'allflag', $uname = 'all', $mstatus = 'notcompleted', $dispType = 'upcoming') {
        $url = $this->base_url . "tasks?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        if (($flag == 'internal') || ($flag == 'external') || ($flag == 'allflag')) {
            //continue;
        } else {
            $this->errors[] = "Invalid Flag parameter ($flag) in getTasks method. Must be allflag,internal or external.";
            return false;
        }
        if (($dispType == 'upcoming') || ($dispType == 'delayed') || ($dispType == 'misc')) {
            //continue;
        } else {
            $this->errors[] = "Invalid Display Type parameter ($dispType) in getTasks method. Must be upcoming, delayed or misc.";
            return false;
        }
        if (($mstatus == 'completed') || ($mstatus == 'notcompleted')) {
            //continue;
        } else {
            $this->errors[] = "Invalid Milestone status parameter ($mstatus) in getTasks method. Must be completed or notcompleted.";
            return false;
        }
        $post = array("projId" => $projId,
            "flag" => $flag,
            "uname" => $uname,
            "mStatus" => $mstatus,
            "dispType" => $dispType
        );
        Return json_decode($this->post_data($url, $post));
    }

    /**
     * get some detailed information about the task
     * @param string $projid - The project ID 
     * @param string $taskId - The task Id
     * @return object return an object containing a bunch of details about a task
     * */
    public function getTaskDetails($projId, $taskId) {
        $url = $this->base_url . "task/content?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        $post = array("projId" => $projId,
            "taskid" => $taskId
        );
        return json_decode($this->post_data($url, $post));
    }

############################### TASK NOTE METHODS ################################################

    /**
     * add a note to a task
     * @param string $projid - The project ID 
     * @param string $taskId - The task Id
     * @param string $noteText - The text of the note
     * @return object return an object containing the note that was added
     * */
    public function addTaskNote($projId, $taskId, $noteText) {
        $url = $this->base_url . "task/addnotes?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        $post = array("projId" => $projId,
            "taskid" => $taskId,
            "notes" => $noteText
        );
        return json_decode($this->post_data($url, $post));
    }

    /**
     * Remove a task note
     * @param string $projid - The project ID 
     * @param string $taskId - The task ID
     * @param string $noteId = The Note ID of the note to be deleted
     * @return boolean return true on success
     * */
    public function deleteTaskNote($projId, $taskId, $noteId) {
        $url = $this->base_url . "task/delnotes?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        $post = array("projId" => $projId,
            "taskid" => $taskId,
            "noteid" => $noteId
        );
        $result = json_decode($this->post_data($url, $post));
        //return json_decode($this->post_data($url,$post));
        if ($result->response->result == 'Deleted Successfully') {
            return true;
        } else {
            $this->errors[] = $this->user . " tried to delete Task ID: " . $taskId . ". This action was unsuccessful.";
            return false;
        }
    }

    /**
     * get the notes associated with a task
     * @param string $projid - The project ID 
     * @param string $taskId - The task Id
     * @return object return an object containing the array of note objects
     * */
    public function getTaskNotes($projId, $taskId) {
        $url = $this->base_url . "task/getnotes?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        $post = array("projId" => $projId,
            "taskid" => $taskId
        );
        return json_decode($this->post_data($url, $post));
    }

################################ DOCUMENT METHODS ################################################

    /**
     * Get all the documents associated with this project
     * @param string $projid - The project ID 
     * @param integer $auditIndex - Starting record for returning tasklists (to be used in paging results)
     * @param integer $range - Number of results to return (to be used in paging results)
     * @return object an object containing an array of document objects
     * */
    public function getDocuments($projId, $auditIndex = '0', $range = '10') {
        $url = $this->base_url . "docs?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        $post = array("projId" => $projId,
            "auditIndex" => $auditIndex,
            "range" => $range
        );
        Return json_decode($this->post_data($url, $post));
    }

    /**
     * Get details for a specific document version (or all versions)
     * @param string $projid - The project ID 
     * @param string $docId - The document ID
     * @param string $version - The specific document version (or specify allversions)
     * @return object an object containing the document details
     * */
    public function getDocumentVersionDetail($projId, $docId, $version = 'allversions') {
        $url = $this->base_url . "doc/version?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        $post = array("projId" => $projId,
            "docId" => $docId,
            "ver" => $version
        );
        Return json_decode($this->post_data($url, $post));
    }

    /**
     * Upload a file to the project
     * @param string $projid - The project ID 
     * @param string $docName - The Name of the document
     * @param string $folderId - The ID of the target folder
     * @param string $tags - a string of tags associated with this file
     * @param string $filepath - The absolute filepath to the file on this server.
     * @return boolean true on success
     * */
    public function addDocument($projId, $docName, $folderId, $tags, $filepath) {
        $url = $this->base_url . "doc/add?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        if (!(file_exists($filepath))) {
            $this->errors[] = "Document Add failed because $filepath does not exist or access permissions were insufficient.";
            return false;
        }
        $post = array("projId" => $projId,
            "docname" => $docName,
            "folderid" => $folderId,
            "tags" => $tags,
            "uploaddoc" => "@" . $filepath
        );
        $result = json_decode($this->post_files($url, $post));
        if ($result->result == 'Request Processed Successfully') {
            return true;
        } else {
            $this->errors[] = $this->user . " tried to upload file " . $filepath . ". This action was unsuccessful.";
            return false;
        }
    }

    /**
     * Upload a new version of an existing document
     * @param string $projid - The project ID 
     * @param string $docName - The Name of the document
     * @param string $folderId - The ID of the target folder
     * @param string $tags - a string of tags associated with this file
     * @param string $filepath - The absolute filepath to the file on this server.
     * @return boolean true on success, check class $errors on fail.
     * */
    public function updateDocument($projId, $docId, $docName, $folderId, $tags, $filepath) {
        $url = $this->base_url . "doc/updlatestver?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        if (!(file_exists($filepath))) {
            $this->errors[] = "Document update failed because $filepath does not exist or access permissions were insufficient.";
            return false;
        }
        // check for matching filenames... the API returns successful with mis-matched filenames but doesn't upload correctly.
        $fname = end(explode(DIRECTORY_SEPARATOR, $filepath));
        $docDetails = $this->getDocumentVersionDetail($projId, $docId);
        $origFname = $docDetails->response->result->DocumentDetails[0]->DocumentDetail->doc_name;
        if ($fname != $origFname) {
            $this->errors[] = "Document updated failed because the new filename($fname) does not match the original filename($origFname).";
            return false;
        }
        $post = array("projId" => $projId,
            "docname" => $docName,
            "folderid" => $folderId,
            "docId" => $docId,
            "tags" => $tags,
            "uploaddoc" => "@" . $filepath
        );
        $result = json_decode($this->post_files($url, $post));
        if ($result->result == 'Request Processed Successfully') {
            return true;
        } else {
            $this->errors[] = $this->user . " tried to upload file " . $filepath . ". This action was unsuccessful.";
            return false;
        }
    }

    /**
     * Delete the document
     * @param string $projid - The project ID 
     * @param string $docId - The document ID
     * @param string $folderId - The folder ID for the document.
     * @return boolean true on successful deletion
     * */
    public function deleteDocument($projId, $docId, $folderId) {
        $url = $this->base_url . "doc/delete?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        $post = array("projId" => $projId,
            "fileid" => $docId,
            "folderid" => $folderId
        );
        $result = json_decode($this->post_data($url, $post));
        if ($result->result == 'Request Processed Successfully') {
            return true;
        } else {
            $this->errors[] = $this->user . " tried to Delete file $docId. This action was unsuccessful.";
            return false;
        }
    }

    /**
     * Edit the tags for a document
     * @param string $projid - The project ID 
     * @param string $docId - The document ID
     * @param string $tags - The new tags in space seperated string
     * @return object an object containing the document details
     * */
    public function updateDocumentTags($projId, $docId, $tags) {
        $url = $this->base_url . "doc/update?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        $post = array("projId" => $projId,
            "docId" => $docId,
            "tags" => $tags
        );
        $result = json_decode($this->post_data($url, $post));
        if ($result->result == 'Request Processed Successfully') {
            return true;
        } else {
            $this->errors[] = $this->user . " tried to update file $docId with the tags ($tags). This action was unsuccessful.";
            return false;
        }
    }

    /**
     * Get all the document tag data for a project
     * @param string $projid - The project ID 
     * @return object an object containing the response
     * */
    public function getProjectTags($projId) {
        $url = $this->base_url . "doc/filetags?apikey=" . $this->api_key . "&ticket=" . $this->authtoken;
        $post = array("projId" => $projId
        );
        Return json_decode($this->post_data($url, $post));
    }

// ******** private functions ***************

    /**
     * Get API AuthToekn for current user from existing file or from zoho server. Store API Ticket for later. API Tickets are specific to a user/pwd credential
     * @return string API ticket
     * @see constant TICKET
     * */
    private function getAuthtoken() {
        $url = 'https://accounts.zoho.com/apiauthtoken/nb/create';
        $request_parameters = array(
            'EMAIL_ID' => $this->user,
            'PASSWORD' => $this->pass,
            'SCOPE' => 'ZohoProjects/projectsapi',
        );
        $request_url = $url . '?' . http_build_query($request_parameters);
        $result = file_get_contents($request_url);
        $anArray = explode("\n", $result);
        $authToken = explode("=", $anArray['2']);
        $cmp = strcmp($authToken['0'], "AUTHTOKEN");
        if ($cmp == 0) {
            $ticket = $authToken['1'];
            return $ticket;
        } else {
            $this->errors[] = "Failed to get API Ticket from Zoho server.";
            return false;
        }
    }

    /**
     * Handles all the cURL API posts, except the file transfers
     * @param string $url URL Endpoint
     * @param string $method_name GET/POST/DELETE
     * @param array $request_parameters Request Object/variables
     * */
    private function request_data($url, $method_name, $request_parameters) {
        $ch = curl_init();
        $request_parameters['authtoken'] = $this->authtoken;
        $request_url = $this->base_url . $url;
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        /* Here you can set all Parameters based on your method_name */
        if ($method_name == 'GET') {
            $request_url .= '?' . http_build_query($request_parameters);
        }
        if ($method_name == 'POST') {
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request_parameters));
        }
        if ($method_name == 'DELETE') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request_parameters));
            $request_url .= '?' . http_build_query($request_parameters);
        }
        /* Here you can set the Response Content Type */
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        /* Let's give the Request Url to Curl */
        curl_setopt($ch, CURLOPT_URL, $request_url);
        /*
          Yes we want to get the Response Header
          (it will be mixed with the response body but we'll separate that after)
         */
        //curl_setopt($ch, CURLOPT_HEADER, TRUE);
        /* Allows Curl to connect to an API server through HTTPS */
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        /* Let's get the Response ! */
        $response = curl_exec($ch);
        /* Don't forget to close Curl */
        curl_close($ch);
        return $response;
    }

    /**
     * Handles the cURL file tranfers
     * */
    private function post_files($url, $post) {
        $ch = curl_init();
        $timeout = 10;
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    /**
     * The zoho api only handles percent complete in multiples of 10. (recursive)
     * This function casts any floating point integer to a multiple of 10 that zoho can interpret correctly.
     * */
    private function cast_pcomplete($per) {
        $per = (float) $per;
        if ($per > 0 && $per <= 1) {
            return $this->cast_pcomplete($per * 100);
        } // actual percent
        $per = round($per, 0);
        switch ($per) {
            case 0:
                return 0;
                break;
            case ($per <= 14):
                return 10;
            case ($per <= 24):
                return 20;
            case ($per <= 34):
                return 30;
            case ($per <= 44):
                return 40;
            case ($per <= 54):
                return 50;
            case ($per <= 64):
                return 60;
            case ($per <= 74):
                return 70;
            case ($per <= 84):
                return 80;
            case ($per <= 94):
                return 90;
            case ($per <= 100):
                return 100;
            default:
                return 0;
        }
    }

}

// end class
?>